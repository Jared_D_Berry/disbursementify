import React from 'react';

export const ContactPage = () => (
    <div className="page">
        <div className="page-header">
            <div className="content-container">
                <div className="header__content">
                    <h1 className="page-header__title">Contact Us</h1>
                </div>
            </div>
        </div>
        <div className="content-container">
            <div className="page__content">
                <p className="page-header__title">Company: Private</p>
                <p className="page__title">Name: Jared Berry</p>
                <p className="page__title">Email: jaredb8@gmail.com</p>
                <p className="page__title">Telegram: 082-912-5050</p>
            </div>
        </div>
    </div>
);

export default ContactPage;