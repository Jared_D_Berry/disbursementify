import React from 'react';

export const HelpPage = () => (
    <div className="page">
        <div className="page-header">
            <div className="content-container">
                <div className="header__content">
                    <h1 className="page-header__title">Help Page</h1>
                </div>

            </div>
        </div>
        <div className="content-container">
            <div className="page__content">
                <p className="page__title">If expenses are not showing up please make sure filters are correctly implemented.</p>
                <p className="page__title">For more help please contact us.</p>
            </div>
        </div>
    </div>
);

export default HelpPage;