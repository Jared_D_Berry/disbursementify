import React from 'react';

export const TermOfServicePage = () => (
    <div className="page">
        <div className="page-header">
            <div className="content-container">
                <div className="header__content">
                    <h1 className="terms__title">Term Of Service</h1>
                </div>
            </div>
        </div>
        <div className="content-container">
            <div className="page__content">
                <h1 className="terms__header">Terms of Service</h1>
                <p className="terms">Please read these terms of service carefully before using Disbursementify website operated by Jared Berry.</p>

                <h3 className="terms__title">Conditions of Use</h3>
                <p className="terms">We will provide their services to you, which are subject to the conditions stated below in this document. Every time you visit this website, use its services, you accept the following conditions. This is why we urge you to read them carefully.</p>

                <h3 className="terms__title__title">Privacy Policy</h3>
                <p className="terms">Before you continue using our website we advise you to read our privacy policy regarding our user data collection. It will help you better understand our practices</p>

                <h3 className="terms__title">Communications</h3>
                <p className="terms">The entire communication with us is electronic. Every time you contact us or visit our website, you are going to be communicating with us. You hereby consent to receive communications from us. We will continue to communicate with you by posting notices on our website. You also agree that all notices, disclosures, agreements and other communications we provide to you electronically meet the legal requirements that such communications be in writing.</p>

                <h3 className="terms__title">Applicable Law</h3>
                <p className="terms">By visiting this website, you agree that the laws of the [your location], without regard to principles of conflict laws, will govern these terms of service, or any dispute of any sort that might come between Disbursementify and you, or its business partners and associates.</p>

                <h3 className="terms__title">Comments, Reviews, and Emails</h3>
                <p className="terms">Visitors may post content as long as it is not obscene, illegal, defamatory, threatening, infringing of intellectual property rights, invasive of privacy or injurious in any other way to third parties. Content has to be free of software viruses, political campaign, and commercial solicitation. We reserve all rights (but not the obligation) to remove and/or edit such content. When you post your content, you grant [name] non-exclusive, royalty-free and irrevocable right to use, reproduce, publish, modify such content throughout the world in any media.</p>

                <h3 className="terms__title">License and Site Access</h3>
                <p className="terms">We grant you a limited license to access and make personal use of this website. You are not allowed to download or modify it. This may be done only with written consent from us.</p>

                <h3 className="terms__title">User Account</h3>
                <p className="terms">If you are an owner of an account on this website, you are solely responsible for maintaining the confidentiality of your private user details (username and password). You are responsible for all activities that occur under your account or password. We reserve all rights to terminate accounts.</p>
            </div>
        </div>
    </div>
);

export default TermOfServicePage;