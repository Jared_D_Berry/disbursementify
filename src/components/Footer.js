import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
    <Footer className="bottom-page">
        <div className="content-container">
            <div className="bottom-page__content">
                <Link className="bottom-page__link" to="/Help">Help</Link>
                <Link className="bottom-page__link" to="/Contact">Contact Us</Link>
                <Link className="bottom-page__link" to="/TermOfService">Term of Service</Link>
                <h6 className="bottom-page__copyright">&copy; Copyright 2017 by Andrew Mead. Feel free to use this project for your own purposes. This does NOT apple if you plan to produce your own course or tutorials based on this project. &copy; 2021 by Disbursementify / Expensify. All right reserved. Built by Jared Berry for my online course, The Complete React Developer Course (w/ Hooks and Redux). Andrew Mead. You are 100% allowed to use this webpage for both personal and commercial use, but NOT to claim it as your own design. A credit to the original author, Andrew Mead, is of course highly appreciated!</h6>
            </div>
        </div>
    </Footer>
);

export default Footer;